# Archivage numérique pour les nuls (Wikibook)

Le groupe de réflexion suisse romand "PatNum" vise depuis 2022 à offrir un espace d'échange entre les institutions membres dans le domaine de l'archivage digital.

 L'un des résultat de leurs travaux est ce manuel simplifié pour mener des projets d'archivage numérique.

L'idée est qu'à terme une partie de ces information puissent être publiées sur la plateforme [Wikibook en français](https://fr.wikibooks.org/wiki/Accueil) .

## Plan du manuel

On propose de découper un projet d'archivage numérique dans les étapes suivantes :
1. **[Signalement](Signalement.md)** : un lot de documents numériques est identifié comme potentiellement archivable numréiquement, prise de contact, inventaire sommaire 1 des ressources et listing des contraintes techniques sur place.
1. **[Capture](Capture.md)** : on procéde à l'acquisition des documents en vue de leur examen (-> salle de tri), voir procédure de copie.
1. **[Analyse](Analyse.md)** : inventaire sommaire 2 des ressources, extraction de l'arborescence, identification des formats
1. **[Décision](Decision.md)** : un choix des documents pertinents est établi, un inventaire sommaire 3 de versement est réalisé, une convention de versement est signée.
1. **[Preservation](Preservation.md)** : on opère la sélection, on convertit les formats, on stocke de façon intermédiaire -> SIP
1. **[Ingest](Ingest.md)** : transfert des SIP dans le dépôt numérique (AIP) 
1. **[Diffusion](Diffusion.md)** : transfert des métadonnées de descriptive dans le AIS (avec le PID), Conversion des AIP en DIP, ajout des métadonnées embarquées dans les Dissemination Copy (DC), édition des Manifest IIIF, transfert des DC sur le IIIF Serveur.
1. **[Mise à jour](Update.md)** : ajout de métadonnées, migration des formats (AIP) (preservation planning)

