# Signalement


Le signalement est la première étape d'un processus d'archivage numérique.

Il s'agit d'identifier un lot de documents potentiellement archivable.
