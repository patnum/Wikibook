# Capture

Voir : [Table des matières](README.md)

Règle à suivre :
- On ne travaille jamais sur les données originales. On doit en établir une copie conforme au préalable.
- la copie conforme doit être établie par une copie par [image dique ou image ISO](/Annexes/Image ISO.md) des données.
- Les copier/coller sont à proscrire car ils effacent les métadonnées (entre autres les dates) liées à l'enregistrement original du fichier.
